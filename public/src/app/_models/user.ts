﻿export class User {
  id: number;
  email: string;
  password: string;
  firstname?: string;
  lastname?: string;
  nickname?: string;
  trigram?: string;
  accessToken?: string;
  refreshToken?: string;
}
