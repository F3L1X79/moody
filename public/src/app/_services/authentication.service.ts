﻿import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {User} from '@app/_models';
import {environment} from '@environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;

  constructor(private router: Router, private http: HttpClient) {
    this.userSubject = new BehaviorSubject<User>(null);
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): User {
    return this.userSubject.value;
  }

  login(email: string, password: string) {
    return this.http
      .post<any>(`${environment.apiUrl}/users/refresh-login`, {email, password})
      .pipe(
        map(user => {
          this.userSubject.next(user);
          this.startRefreshTokenTimer();
          return user;
        }),
      );
  }

  logout() {
    this.http
      .delete<any>(
        `${environment.apiUrl}/users/revoke-token/` +
          this.userValue?.accessToken,
      )
      .subscribe();
    this.stopRefreshTokenTimer();
    this.userSubject.next(null);
    this.router.navigate(['/login']);
  }

  refreshToken() {
    const tokenAsACookie = (
      document.cookie.split(';').find(x => x.includes('moodyRefreshToken')) ||
      '='
    ).split('=')[1];
    return this.http
      .post<any>(`${environment.apiUrl}/users/refresh-token`, {
        refreshToken: tokenAsACookie,
      })
      .pipe(
        map(user => {
          this.userSubject.next(user);
          this.startRefreshTokenTimer();
          return user;
        }),
      );
  }

  // helper methods

  private refreshTokenTimeout;

  private startRefreshTokenTimer() {
    // parse json object from base64 encoded jwt token
    const token = JSON.parse(atob(this.userValue.accessToken.split('.')[1]));

    const expires = new Date(token.exp * 1000);
    // save it as a cookie
    document.cookie = `moodyRefreshToken=${this.userValue.refreshToken}; expires=${expires}; path=/`;
    // set a timeout to refresh the token a minute before it expires
    const timeout = expires.getTime() - Date.now() - 60 * 1000;
    this.refreshTokenTimeout = setTimeout(
      () => this.refreshToken().subscribe(),
      timeout,
    );
  }

  private stopRefreshTokenTimer() {
    clearTimeout(this.refreshTokenTimeout);
  }
}
