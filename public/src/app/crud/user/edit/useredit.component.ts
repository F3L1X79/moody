﻿import {Component} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {User} from '@app/_models';
import {UserService} from '@app/_services';
import {first} from 'rxjs/operators';

@Component({templateUrl: 'useredit.component.html'})
export class UserEditComponent {
  loading = false;
  user: User;
  add = false;

  constructor(private route: ActivatedRoute, private userService: UserService) {
    this.user = new User();
  }

  ngOnInit() {
    this.loading = true;
    this.route.paramMap.subscribe((params: ParamMap) => {
      let userId = params.get('id');
      if (userId) {
        return this.userService
          .getById(userId)
          .pipe(first())
          .subscribe(user => {
            this.loading = false;
            this.user = user;
          });
      }
      // add mode
      else {
        this.add = true;
      }
    });
  }
}
