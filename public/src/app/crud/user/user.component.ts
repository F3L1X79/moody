﻿import {Component} from '@angular/core';
import {User} from '@app/_models';
import {UserService} from '@app/_services';
import {first} from 'rxjs/operators';

@Component({templateUrl: 'user.component.html'})
export class UserComponent {
  loading = false;
  users: User[];

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.loading = true;
    this.userService
      .getAll()
      .pipe(first())
      .subscribe(users => {
        this.loading = false;
        this.users = users;
      });
  }
}
