﻿import {Component} from '@angular/core';
import {User} from './_models';
import {AuthenticationService} from './_services';

@Component({selector: 'app', templateUrl: 'app.component.html'})
export class AppComponent {
  user: User;

  constructor(private authenticationService: AuthenticationService) {
    this.authenticationService.user.subscribe(x => (this.user = x));
  }

  logout() {
    this.authenticationService.logout();
  }
}
