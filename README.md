
## WARNING: ⚠️Work In Progress⚠️

> This is a Work In Progress app,
> not designed for production

# Moody : the mood board webapp

- Powered by LoopbackJs 4 & Angular 10

## Installation

- Récupérer toutes les sources du projet (_.zip_ ou via _git repository_).
- Installer _nodeJs_ et _npm_ globalement : https://www.npmjs.com/get-npm
- Installer toutes les dépendances npm nécessaires au projet - exécuter à la racine : `npm install`

  Si cela a correctement fonctionné vous devriez avoir deux nouveaux dossiers _node_modules_ : Un à la racine et l'autre dans le dossier `/public` nécessaire au frontend.

- Créer une base locale intutilée 'moody' (système MySQL).

## Lancement (localhost)

Exécuter `npm run dev` à la racine.

- Frontend Angular : Naviguer sur : `http://localhost:4200`
- Accès au Swagger de l'API : Naviguer sur : `http://localhost:3000/explorer`
