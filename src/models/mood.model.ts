import {Entity, hasMany, model, property} from '@loopback/repository';
import {Daymood} from './daymood.model';
import {User} from './user.model';

@model()
export class Mood extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: 'uuidv4',
  })
  id: string;

  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'string',
    required: true,
  })
  iconPath: string;

  @hasMany(() => User, {through: {model: () => Daymood}})
  users: User[];

  constructor(data?: Partial<Mood>) {
    super(data);
  }
}

export interface MoodRelations {
  // describe navigational properties here
}

export type MoodWithRelations = Mood & MoodRelations;
