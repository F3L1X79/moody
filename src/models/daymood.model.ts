import {Entity, model, property} from '@loopback/repository';

@model({
  /*
  settings: {
    foreignKeys: {
      FK_DAYMOOD_USERID: {
        name: 'fk_daymood_userId',
        entity: 'User',
        entityKey: 'id',
        foreignKey: 'userId',
      },
      FK_DAYMOOD_MOODID: {
        name: 'fk_daymood_moodId',
        entity: 'Mood',
        entityKey: 'id',
        foreignKey: 'moodId',
      },
    },
  },
  */
})
export class Daymood extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: 'uuidv4',
  })
  id: string;

  @property({
    type: 'date',
    required: true,
  })
  day: string;

  @property({
    type: 'string',
    required: true,
  })
  userId: string;

  @property({
    type: 'string',
    required: true,
  })
  moodId: string;

  constructor(data?: Partial<Daymood>) {
    super(data);
  }
}

export interface DaymoodRelations {
  // describe navigational properties here
}

export type DaymoodWithRelations = Daymood & DaymoodRelations;
