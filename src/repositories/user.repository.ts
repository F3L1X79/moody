import {
  UserCredentials,
  UserCredentialsRepository,
} from '@loopback/authentication-jwt';
import {Getter, inject} from '@loopback/core';
import {
  DefaultCrudRepository,
  HasManyThroughRepositoryFactory,
  HasOneRepositoryFactory,
  repository,
} from '@loopback/repository';
import {MoodyDsDataSource} from '../datasources';
import {Daymood, Mood, User, UserRelations} from '../models';
import {DaymoodRepository} from './daymood.repository';
import {MoodRepository} from './mood.repository';

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {
  public readonly moods: HasManyThroughRepositoryFactory<
    Mood,
    typeof Mood.prototype.id,
    Daymood,
    typeof User.prototype.id
  >;
  public readonly userCredentials: HasOneRepositoryFactory<
    UserCredentials,
    typeof User.prototype.id
  >;

  constructor(
    @inject('datasources.moodyDs') dataSource: MoodyDsDataSource,
    @repository.getter('DaymoodRepository')
    protected daymoodRepositoryGetter: Getter<DaymoodRepository>,
    @repository.getter('MoodRepository')
    protected moodRepositoryGetter: Getter<MoodRepository>,
    @repository.getter('UserCredentialsRepository')
    protected userCredentialsRepositoryGetter: Getter<
      UserCredentialsRepository
    >,
  ) {
    super(User, dataSource);
    this.moods = this.createHasManyThroughRepositoryFactoryFor(
      'moods',
      moodRepositoryGetter,
      daymoodRepositoryGetter,
    );
    this.userCredentials = this.createHasOneRepositoryFactoryFor(
      'userCredentials',
      userCredentialsRepositoryGetter,
    );
    this.registerInclusionResolver(
      'userCredentials',
      this.userCredentials.inclusionResolver,
    );
  }

  async findCredentials(
    userId: typeof User.prototype.id,
  ): Promise<UserCredentials | undefined> {
    try {
      return await this.userCredentials(userId).get();
    } catch (err) {
      if (err.code === 'ENTITY_NOT_FOUND') {
        return undefined;
      }
      throw err;
    }
  }
}
