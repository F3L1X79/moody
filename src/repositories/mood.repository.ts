import {DefaultCrudRepository, repository, HasManyThroughRepositoryFactory} from '@loopback/repository';
import {Mood, MoodRelations, User, Daymood} from '../models';
import {MoodyDsDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {DaymoodRepository} from './daymood.repository';
import {UserRepository} from './user.repository';

export class MoodRepository extends DefaultCrudRepository<
  Mood,
  typeof Mood.prototype.id,
  MoodRelations
> {

  public readonly users: HasManyThroughRepositoryFactory<User, typeof User.prototype.id,
          Daymood,
          typeof Mood.prototype.id
        >;

  constructor(
    @inject('datasources.moodyDs') dataSource: MoodyDsDataSource, @repository.getter('DaymoodRepository') protected daymoodRepositoryGetter: Getter<DaymoodRepository>, @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(Mood, dataSource);
    this.users = this.createHasManyThroughRepositoryFactoryFor('users', userRepositoryGetter, daymoodRepositoryGetter,);
  }
}
