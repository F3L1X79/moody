import {DefaultCrudRepository} from '@loopback/repository';
import {RefreshToken, RefreshTokenRelations} from '../models';
import {MoodyDsDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class RefreshTokenRepository extends DefaultCrudRepository<
  RefreshToken,
  typeof RefreshToken.prototype.id,
  RefreshTokenRelations
> {
  constructor(
    @inject('datasources.moodyDs') dataSource: MoodyDsDataSource,
  ) {
    super(RefreshToken, dataSource);
  }
}
