import {DefaultCrudRepository} from '@loopback/repository';
import {Daymood, DaymoodRelations} from '../models';
import {MoodyDsDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DaymoodRepository extends DefaultCrudRepository<
  Daymood,
  typeof Daymood.prototype.id,
  DaymoodRelations
> {
  constructor(
    @inject('datasources.moodyDs') dataSource: MoodyDsDataSource,
  ) {
    super(Daymood, dataSource);
  }
}
