import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {Mood, User} from '../models';
import {UserRepository} from '../repositories';

@authenticate('jwt')
export class UserMoodController {
  constructor(
    @repository(UserRepository) protected userRepository: UserRepository,
  ) {}

  @get('/users/{id}/moods', {
    responses: {
      '200': {
        description: 'Array of User has many Mood through Daymood',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Mood)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Mood>,
  ): Promise<Mood[]> {
    return this.userRepository.moods(id).find(filter);
  }

  @post('/users/{id}/moods', {
    responses: {
      '200': {
        description: 'create a Mood model instance',
        content: {'application/json': {schema: getModelSchemaRef(Mood)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof User.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Mood, {
            title: 'NewMoodInUser',
            exclude: ['id'],
          }),
        },
      },
    })
    mood: Omit<Mood, 'id'>,
  ): Promise<Mood> {
    return this.userRepository.moods(id).create(mood);
  }

  @patch('/users/{id}/moods', {
    responses: {
      '200': {
        description: 'User.Mood PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Mood, {partial: true}),
        },
      },
    })
    mood: Partial<Mood>,
    @param.query.object('where', getWhereSchemaFor(Mood)) where?: Where<Mood>,
  ): Promise<Count> {
    return this.userRepository.moods(id).patch(mood, where);
  }

  @del('/users/{id}/moods', {
    responses: {
      '200': {
        description: 'User.Mood DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Mood)) where?: Where<Mood>,
  ): Promise<Count> {
    return this.userRepository.moods(id).delete(where);
  }
}
