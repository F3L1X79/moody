import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
} from '@loopback/rest';
import {Mood} from '../models';
import {MoodRepository} from '../repositories';
@authenticate('jwt')
export class MoodController {
  constructor(
    @repository(MoodRepository)
    public moodRepository: MoodRepository,
  ) {}

  @post('/moods', {
    responses: {
      '200': {
        description: 'Mood model instance',
        content: {'application/json': {schema: getModelSchemaRef(Mood)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Mood, {
            title: 'NewMood',
          }),
        },
      },
    })
    mood: Mood,
  ): Promise<Mood> {
    return this.moodRepository.create(mood);
  }

  @get('/moods/count', {
    responses: {
      '200': {
        description: 'Mood model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(Mood) where?: Where<Mood>): Promise<Count> {
    return this.moodRepository.count(where);
  }

  @get('/moods', {
    responses: {
      '200': {
        description: 'Array of Mood model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Mood, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(@param.filter(Mood) filter?: Filter<Mood>): Promise<Mood[]> {
    return this.moodRepository.find(filter);
  }

  @patch('/moods', {
    responses: {
      '200': {
        description: 'Mood PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Mood, {partial: true}),
        },
      },
    })
    mood: Mood,
    @param.where(Mood) where?: Where<Mood>,
  ): Promise<Count> {
    return this.moodRepository.updateAll(mood, where);
  }

  @get('/moods/{id}', {
    responses: {
      '200': {
        description: 'Mood model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Mood, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Mood, {exclude: 'where'}) filter?: FilterExcludingWhere<Mood>,
  ): Promise<Mood> {
    return this.moodRepository.findById(id, filter);
  }

  @patch('/moods/{id}', {
    responses: {
      '204': {
        description: 'Mood PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Mood, {partial: true}),
        },
      },
    })
    mood: Mood,
  ): Promise<void> {
    await this.moodRepository.updateById(id, mood);
  }

  @put('/moods/{id}', {
    responses: {
      '204': {
        description: 'Mood PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() mood: Mood,
  ): Promise<void> {
    await this.moodRepository.replaceById(id, mood);
  }

  @del('/moods/{id}', {
    responses: {
      '204': {
        description: 'Mood DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.moodRepository.deleteById(id);
  }
}
